FROM richarvey/nginx-php-fpm:latest

LABEL description="Rainloop is a simple, modern & fast web-based client" \
   application="rainloop"

ARG GPG_FINGERPRINT="3B79 7ECE 694F 3B7B 70F3  11A4 ED7C 49D9 87DA 4591"

RUN apk add openldap \
   rsync

RUN apk add gnupg \
   openldap-dev \
   --virtual build-dependencies

RUN docker-php-ext-install ldap simplexml

WORKDIR /tmp

RUN wget -q https://www.rainloop.net/repository/webmail/rainloop-community-latest.zip \
   && wget -q https://www.rainloop.net/repository/webmail/rainloop-community-latest.zip.asc \
   && wget -q https://www.rainloop.net/repository/RainLoop.asc \
   && gpg --import RainLoop.asc \
   && FINGERPRINT="$(LANG=C gpg --verify rainloop-community-latest.zip.asc rainloop-community-latest.zip 2>&1 \
   | sed -n "s#Primary key fingerprint: \(.*\)#\1#p")" \
   && if [ -z "${FINGERPRINT}" ]; then echo "ERROR: Invalid GPG signature!" && exit 1; fi \
   && if [ "${FINGERPRINT}" != "${GPG_FINGERPRINT}" ]; then echo "ERROR: Wrong GPG fingerprint!" && exit 1; fi \
   && unzip -qo /tmp/rainloop-community-latest.zip -d /var/www/html \
   && rm -rf /tmp/* /var/cache/apk/* /root/.gnupg

RUN apk del build-dependencies 


COPY rootfs /
RUN chmod a+x /start.sh
VOLUME /var/www/html/data

# forward request and error logs to docker log collector
RUN ln -sf /dev/stdout /var/log/nginx/access.log \
   && ln -sf /dev/stderr /var/log/nginx/error.log

LABEL maintainer="Marvin Roman <marvinroman@protonmail.com>"
LABEL version="0.0.5"

EXPOSE 443 80
CMD ["/start.sh"]
